﻿using NUnit.Framework;

namespace StringCalculator.NunitTests
{
    public class CalculatorNunitTests
    {
        [Test]
        public void EmptyStringReturnsZero()
        {
            var result = (new Calculator()).Add("");

            Assert.AreEqual(0, result);
        }

        [Test]
        public void SingleDigitReturnsValue()
        {
            var result = (new Calculator()).Add("1");

            Assert.AreEqual(1, result);
        }
    }
}
