﻿using Xunit;

namespace StringCalculator.XnuitTests
{
    public class CalculatorXunitTests
    {
        [Fact]
        public void EmptyStringReturnsZero()
        {
            var result = (new Calculator()).Add("");

            Assert.Equal(0, result);
        }

        [Fact]
        public void SingleDigitReturnsValue()
        {
            var result = (new Calculator()).Add("1");

            Assert.Equal(1, result);
        }
    }
}
