using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace StringCalculator.Tests
{
    [TestClass]
    public class CalculatorMSTestTests
    {
        [TestMethod]
        public void EmptyStringReturnsZero()
        {
            var result = (new Calculator()).Add("");

            Assert.AreEqual(0, result);
        }

        [TestMethod]
        public void SingleDigitReturnsValue()
        {
            var result = (new Calculator()).Add("1");

            Assert.AreEqual(1, result);
        }
    }
}
